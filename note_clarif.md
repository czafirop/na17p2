# Note de clarif

## Article

- Auteur
- Statut : {en rédaction, soumis, en relecture, rejeté, à réviser, validé}
- Etat : {actif, supprimé}
- Titre
- date de creation
- date de publication
- justification/preconisation 1 seul car depend du statut
- Rubrique [1..n]
- liste de mots clé[*]
- Blocs[*]
- commentaire [*]
- lien vers d'autre articles[*]
- a l'honneur (booleen)

## Rubrique

- libélé
- description
- pere[1]
- Sous rubrique[*]
- Article [*]

## Mots clé

- nom

## Blocs

- numero
- Titre
- Image ou Texte

## Commentaire

- lié a un article
- titre
- Etat : {masqué, normal, en exergue, supprimé}
- date
- texte
- Auteur

## utilisateur

- id
- pseudo
- mot de passe
- date de creation du compte
- nom
- prenom
- mail
- type :
  - libélé (Administrateur, Auteur, Editeur, Lecteur, Moderateur)

## droits(hypothese)

- 2 type
  - intrinseque au type
  - droit suplementaire
- possede un libelé

## action

- type d'action

## log

- id_utilisateur
- id_action
- id_entité modifié
- date

## Utilisateur

- Administrateur
  - creer compte
  - supprimer compte
- Auteur
  - créer article
  - ajouter blocs dans un de ses article
  - voir ses article
  - supprimer un article
  - recuperer un article supprimé
  - passer de soumis "en redaction" a "soumis"
- Editeur
  - voir tous les articles
  - statut de *soumis* a :
    - *en relecture* (noté qui relie)
    - *rejeté* (avec une justification)
    - *a reviser* (préconisation)
    - *validé* (pret a etre publié)
  - ajouter des mots clé au articles
  - article dans [1..n] rubrique
  - publier des articles (check si valide)
  - mettre les article a l'honneur et enlever
  - lier les articles en eux (lien entre 2 article)
- Lecteur
  - lire un article
  - chercher dans l'arborescence des rubrique
  - chercher par mot clé
  - accés a categorie a l'honneur
  - commenter un article
  - supprimer un de ses articles
  - voir les commentaire (visible)
- Moderateur (uniquement commentaire)
  - masquer un commentaire
  - supprimer un commentaire
  - mettre en exergue un commentaire
