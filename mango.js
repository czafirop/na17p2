db.articles.drop()
db.articles.insert([
    {
        titre : "L'art de la table pour les nuls",
        auteur:
            {
                nom: "ABRVEC", 
                prenom: "Sliman"
            },
        statut: "EN REDACTION", 
        etat: "ACTIF",
        date_creation: "18-02-2018",
        note_editeur:"c'est bien mon gars bon boulot",
        honneur: "FALSE",
        blocs:[
                {
                    titre:"Premierement",
                    ordre:1,
                    contenu:"https://bonjour.com/premier"
                },{
                    titre:"deuxiemement",
                    ordre:2,
                    contenu:"https://bonjour.com/deux"
                }
            ]
    },{
        titre : "La magie les revelations",
        auteur:
            {
                nom: "ABRVEC", 
                prenom: "Jia min"
            },
        statut: "PUBLIE", 
        etat: "ACTIF",
        date_creation: "19-02-2018",
        date_publication: "26-03-2019",
        note_editeur:"faute",
        honneur: "TRUE",
        blocs:[
                {
                    titre:"Premierement",
                    ordre:1,
                    contenu:"https://bonjour.com/premierement"
                },{
                    titre:"deuxiemement",
                    ordre:2,
                    contenu:"blablabla"
                }
            ]
    },{
        titre : "L'histoire de harry",
        auteur:
            {
                nom: "ABRVEC", 
                prenom: "Jia min"
            },
        statut: "PUBLIE", 
        etat: "ACTIF",
        date_creation: "19-02-2019",
        date_publication: "20-02-2019",
        note_editeur:"Un café ce soir?",
        honneur: "FALSE",
        blocs:[
                {
                    titre:"Premierement",
                    ordre:1,
                    contenu:"https://bonjour.com/premierement"
                },{
                    titre:"deuxiemement",
                    ordre:2,
                    contenu:"blablablaoulalal bonjour oui"
                }
            ]
    },{
        titre : "L'histoire des gens heureux",
        auteur:
            {
                nom: "LABALADE", 
                prenom: "Desgens"
            },
        statut: "PUBLIE", 
        etat: "ACTIF",
        date_creation: "19-03-2019",
        date_publication: "20-06-2019",
        note_editeur:"HEUREUX",
        honneur: "TRUE",
        blocs:[
                {
                    titre:"Premierement",
                    ordre:1,
                    contenu:"https://bonjour.com/premierement"
                },{
                    titre:"deuxiemement",
                    ordre:2,
                    contenu:"blablabla et puis les voitures et puis les avions"
                },{
                    titre:"troilala",
                    ordre:3,
                    contenu:"Bonjour c'est le troisieme bloc"
                }
            ]
    }
]);

db.articles.count();
db.articles.find({"honneur":"TRUE"});
db.articles.find({"auteur.nom":"ABRVEC","auteur.prenom":"Jia min"});