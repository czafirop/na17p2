DROP TABLE IF EXISTS log_user;
DROP TABLE IF EXISTS action_user;
DROP TABLE IF EXISTS log_article;
DROP TABLE IF EXISTS action_article;
DROP TABLE IF EXISTS log_commentaire;
DROP TABLE IF EXISTS action_commentaire;
DROP TABLE IF EXISTS commentaire;
DROP TABLE IF EXISTS etat_commentaire;
DROP TABLE IF EXISTS blocs_image;
DROP TABLE IF EXISTS blocs_text;
DROP TABLE IF EXISTS mot_article;
DROP TABLE IF EXISTS mot_cle;
DROP TABLE IF EXISTS lien_article;
DROP TABLE IF EXISTS rubrique_article;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS etat_article;
DROP TABLE IF EXISTS statut_article;
DROP TABLE IF EXISTS arbre_rubrique;
DROP TABLE IF EXISTS rubrique;
DROP TABLE IF EXISTS droit_user;
DROP TABLE IF EXISTS droit_type;
DROP TABLE IF EXISTS droits;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS user_type;

CREATE TABLE user_type(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE users(
    id INTEGER NOT NULL CHECK (id>0),
    mot_de_passe VARCHAR NOT NULL,
    pseudo VARCHAR NOT NULL,
    nom VARCHAR,
    prenom VARCHAR,
    mail VARCHAR NOT NULL,
    usertype VARCHAR NOT NULL,
    FOREIGN KEY (usertype) REFERENCES user_type (libelle),
    PRIMARY KEY(id)
);

CREATE TABLE droits(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE droit_type(
    usertype VARCHAR NOT NULL,
    droits VARCHAR NOT NULL,
    FOREIGN KEY (usertype) REFERENCES user_type (libelle),
    FOREIGN KEY (droits) REFERENCES droits (libelle),
    PRIMARY KEY(usertype,droits)
);

CREATE TABLE droit_user(
    users INTEGER NOT NULL,
    droits VARCHAR NOT NULL,
    FOREIGN KEY (users) REFERENCES users (id),
    FOREIGN KEY (droits) REFERENCES droits (libelle),
    PRIMARY KEY (users,droits)
);

CREATE TABLE rubrique(
    libelle VARCHAR NOT NULL,
    texte TEXT,
    PRIMARY KEY (libelle)
);

CREATE TABLE arbre_rubrique(
    pere VARCHAR NOT NULL,
    fils VARCHAR NOT NULL,
    FOREIGN KEY (pere) REFERENCES rubrique (libelle),
    FOREIGN KEY (fils) REFERENCES rubrique (libelle),
    PRIMARY KEY (pere, fils)
);

CREATE TABLE statut_article(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE etat_article(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE article(
    id INTEGER NOT NULL CHECK (id>0),
    auteur INTEGER NOT NULL,
    statut VARCHAR NOT NULL,
    etat VARCHAR NOT NULL,
    titre VARCHAR,
    date_creation DATE NOT NULL,
    date_publication DATE,
    note_editeur TEXT,
    honneur BOOLEAN,
    FOREIGN KEY (etat) REFERENCES etat_article(libelle),
    FOREIGN KEY (statut) REFERENCES statut_article(libelle),
    FOREIGN KEY (auteur) REFERENCES users (id),
    PRIMARY KEY(id)
);

CREATE TABLE rubrique_article(
    rubrique VARCHAR NOT NULL,
    article INTEGER NOT NULL,
    FOREIGN KEY (rubrique) REFERENCES rubrique(libelle),
    FOREIGN KEY (article) REFERENCES article(id),
    PRIMARY KEY(rubrique,article)
);

CREATE TABLE lien_article(
    article1 INTEGER NOT NULL,
    article2 INTEGER NOT NULL,
    FOREIGN KEY (article1) REFERENCES article (id),
    FOREIGN KEY (article2) REFERENCES article (id),
    PRIMARY KEY(article1,article2)
);

CREATE TABLE mot_cle(
    mot VARCHAR NOT NULL,
    PRIMARY KEY(mot)
);

CREATE TABLE mot_article(
    mot VARCHAR NOT NULL,
    article INTEGER NOT NULL,
    PRIMARY KEY(mot,article)
);

CREATE TABLE blocs_text(
    id INTEGER NOT NULL CHECK (id>0),
    article INTEGER NOT NULL,
    numero INTEGER NOT NULL CHECK (id>0),
    titre VARCHAR,
    contenu TEXT,
    FOREIGN KEY (article) REFERENCES article (id),
    PRIMARY KEY(id)
);

CREATE TABLE blocs_image(
    id INTEGER NOT NULL CHECK (id>0),
    article INTEGER NOT NULL,
    numero INTEGER NOT NULL CHECK (id>0),
    titre VARCHAR,
    lien VARCHAR,
    FOREIGN KEY (article) REFERENCES article (id),
    PRIMARY KEY(id)
);

CREATE TABLE etat_commentaire(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE commentaire(
    id INTEGER NOT NULL CHECK (id>0),
    date_creation DATE NOT NULL,
    titre VARCHAR,
    texte TEXT,
    auteur INTEGER NOT NULL,
    article INTEGER NOT NULL,
    etat VARCHAR NOT NULL,
    FOREIGN KEY (article) REFERENCES article (id),
    FOREIGN KEY (auteur) REFERENCES users (id),
    FOREIGN KEY (etat) REFERENCES etat_commentaire (libelle),
    PRIMARY KEY(id)
);

CREATE TABLE action_commentaire(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE log_commentaire(
    id INTEGER NOT NULL CHECK (id>0),
    temps TIMESTAMP NOT NULL,
    objet INTEGER NOT NULL,
    users INTEGER NOT NULL,
    action VARCHAR NOT NULL,
    FOREIGN KEY (objet) REFERENCES commentaire (id),
    FOREIGN KEY (users) REFERENCES users (id),
    FOREIGN KEY (action) REFERENCES action_commentaire (libelle),
    PRIMARY KEY(id)
);

CREATE TABLE action_article(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE log_article(
    id INTEGER NOT NULL CHECK (id>0),
    temps TIMESTAMP NOT NULL,
    objet INTEGER NOT NULL,
    users INTEGER NOT NULL,
    action VARCHAR NOT NULL,
    FOREIGN KEY (objet) REFERENCES article (id),
    FOREIGN KEY (users) REFERENCES users (id),
    FOREIGN KEY (action) REFERENCES action_article (libelle),
    PRIMARY KEY(id)
);

CREATE TABLE action_user(
    libelle VARCHAR NOT NULL,
    PRIMARY KEY(libelle)
);

CREATE TABLE log_user(
    id INTEGER NOT NULL CHECK (id>0),
    temps TIMESTAMP NOT NULL,
    objet INTEGER NOT NULL,
    users INTEGER NOT NULL,
    action VARCHAR NOT NULL,
    FOREIGN KEY (objet) REFERENCES users (id),
    FOREIGN KEY (users) REFERENCES users (id),
    FOREIGN KEY (action) REFERENCES action_user (libelle),
    PRIMARY KEY(id)
);

