# MLD

## Users

User(#id : posint, mot_de_passe : Varchar NOT NULL, pseudo : Varchar NOT NULL, nom : Varchar, prenom : Varchar, mail : Varchar NOT NULL, Type => UserType.libelle NOT NULL)

UserType(#libelle : Varchar)

## Droits

Droit(#libelle : Varchar)

DroitType(#Type => UserType.libelle, #Droit => Droit.libelle)

DroitUser(#User => User.id, #Droit=> Droit.libelle)

## Articles

Rubrique(#libelle : Varchar, description : text)

Arbre_rubrique(#pere =>rubrique.libele, #fils=>rubrique.libelle)

StatutArticle(#libelle : Varchar)

EtatArtcile(#libelle : Varchar)

Article(#id : posint,
        auteur => user.id NOT NULL,
        statut => StatutArticle.libelle NOT NULL,
        etat =>EtatArticle.libelle NOT NULL,
        titre : Varchar,
        date_creation : Date NOT NULL,
        publication : Date,
        note_editeur : text,
        Honneur : bool)

LienArticle(#article1 => Article.id, #article2 => Article.id)

Mot_cle(#mot : Varchar)

Mot_cleArticle(#mot=>Mot_cle.mot, #Article=>Article.id)

BlocsText(#id : posint, article => article.id NOT NULL, numero : posint, titre : Varchar, contenu : text)

BlocsImage(#id : posint, article => article.id NOT NULL, numero : posint, titre : Varchar, lien : Varchar)

## commentaires

EtatCommentaire(#libelle: varchar)

Commentaire(#id : posint, date_creation : date NOT NULL, titre : Varchar, texte : text, etat => EtatCommentaire.libelle NOT NULL, article => article.id NOT NULL, auteur => User.id NOT NULL)

## log

### commentaire

ActionCommentaire(#libelle : Varchar)

LogCommentaire(#id : posint, date : DATETIME NOT NULL, objet => Commentaire.id NOT NULL, user => User.id NOT NULL, action => ActionCommentaire.id NOT NULL)

### Article

ActionArticle(#libelle : Varchar)

LogArticle(#id : posint, date : DATETIME NOT NULL, objet => Article.id NOT NULL, user => User.id NOT NULL, action => ActionArticle.id NOT NULL)

### User

ActionUser(#libelle : Varchar)

LogUser(#id : posint, date : DATETIME NOT NULL, objet => User.id NOT NULL, user =>User.id NOT NULL, action => ActionUser.id NOT NULL)
