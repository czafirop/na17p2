INSERT INTO user_type (libelle) VALUES ('Administrateur');
INSERT INTO user_type (libelle) VALUES ('Auteur');
INSERT INTO user_type (libelle) VALUES ('Editeur');
INSERT INTO user_type (libelle) VALUES ('Lecteur');
INSERT INTO user_type (libelle) VALUES ('Moderateur');

INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(1,'je suis beau','le bg','','','lebeg@gmail.com','Lecteur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(12,'mamanjetaime','lovelycuteplusplus','','','cutiecutie@gmail.com','Lecteur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(2,'123456789','camcam','Camille','Lecossois','Lecossois@gmail.com','Editeur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(3,'987456321','Jazounette','Quoy','Salomé','s.quoy@gmail.com','Auteur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(333,'987456321','THE AUTHOR','aude','javel','aude.javel@blanc.com','Auteur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(4,'gertrude','gerard','Gerard','Bertrand','Gerard.Bertrand@gmail.com','Lecteur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(10,'JETUEDESGENS','SUPER_ADMIN','','','admin@gmail.com','Administrateur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(11,'JESUISGENTIL','SUPER','','','admin_low@gmail.com','Administrateur');
INSERT INTO users(id, mot_de_passe, pseudo, nom, prenom, mail, usertype)
VALUES(150,'LaVieDeVicompteBancaire','JEVAISTEMUTE','Henry','Charles','aquandlheritage@gmail.com','Moderateur');

INSERT INTO droits(libelle) VALUES ('lire');
INSERT INTO droits(libelle) VALUES ('cacher commentaire');
INSERT INTO droits(libelle) VALUES ('creer compte');
INSERT INTO droits(libelle) VALUES ('supprimer compte');
INSERT INTO droits(libelle) VALUES ('publier');
INSERT INTO droits(libelle) VALUES ('passer en mode relecture');
INSERT INTO droits(libelle) VALUES ('creer article');
INSERT INTO droits(libelle) VALUES ('supprimer article');
INSERT INTO droits(libelle) VALUES ('ajouter commentaire');
INSERT INTO droits(libelle) VALUES ('supprimer mes commentaire');
INSERT INTO droits(libelle) VALUES ('supprimer commentaires');
INSERT INTO droits(libelle) VALUES ('voir article non publié');
INSERT INTO droits(libelle) VALUES ('valider un article');
INSERT INTO droits(libelle) VALUES ('mettre a l honneur');

INSERT INTO droit_type(usertype, droits) VALUES ('Lecteur','lire');
INSERT INTO droit_type(usertype, droits) VALUES ('Lecteur','ajouter commentaire');
INSERT INTO droit_type(usertype, droits) VALUES ('Lecteur','supprimer mes commentaire');
INSERT INTO droit_type(usertype, droits) VALUES ('Auteur','lire');
INSERT INTO droit_type(usertype, droits) VALUES ('Auteur','creer article');
INSERT INTO droit_type(usertype, droits) VALUES ('Auteur','supprimer article');
INSERT INTO droit_type(usertype, droits) VALUES ('Auteur','ajouter commentaire');
INSERT INTO droit_type(usertype, droits) VALUES ('Auteur','supprimer mes commentaire');
INSERT INTO droit_type(usertype, droits) VALUES ('Editeur','publier');
INSERT INTO droit_type(usertype, droits) VALUES ('Editeur','passer en mode relecture');
INSERT INTO droit_type(usertype, droits) VALUES ('Editeur','voir article non publié');
INSERT INTO droit_type(usertype, droits) VALUES ('Editeur','valider un article');
INSERT INTO droit_type(usertype, droits) VALUES ('Editeur','mettre a l honneur');
INSERT INTO droit_type(usertype, droits) VALUES ('Editeur','lire');
INSERT INTO droit_type(usertype, droits) VALUES ('Moderateur','lire');
INSERT INTO droit_type(usertype, droits) VALUES ('Moderateur','supprimer commentaires');
INSERT INTO droit_type(usertype, droits) VALUES ('Moderateur','cacher commentaire');
INSERT INTO droit_type(usertype, droits) VALUES ('Administrateur','supprimer compte');
INSERT INTO droit_type(usertype, droits) VALUES ('Administrateur','creer compte');

INSERT INTO droit_user(users, droits) VALUES (10,'lire');


INSERT INTO rubrique(libelle,texte) 
VALUES ('CUISINE','Art de la table pour les plus gourmand');
INSERT INTO rubrique(libelle,texte) 
VALUES ('VOYAGE','Desir de decouvrire le monde');
INSERT INTO rubrique(libelle,texte) 
VALUES ('DECORATION','Art deco concept et innovation');
INSERT INTO rubrique(libelle,texte) 
VALUES ('POLITIQUE','Les politique les plus vereux vous raconte leurs histoires');
INSERT INTO rubrique(libelle,texte) 
VALUES ('SCIENCES','Sans concience nest que ruine de lâme');
INSERT INTO rubrique(libelle,texte) 
VALUES ('ECONOMIE','Largent ne fait pas le bonheur');
INSERT INTO rubrique(libelle,texte) 
VALUES ('ENVIRONEMENT','Sauvegarde du monde');
INSERT INTO rubrique(libelle,texte) 
VALUES ('SANTE','Le magasine pour se rendre malade');
INSERT INTO rubrique(libelle,texte) 
VALUES ('MODE','Cest beau et ça change vite');
INSERT INTO rubrique(libelle,texte)
VALUES ('BIEN ETRE','Rentrez en phase avec vous meme');
INSERT INTO rubrique(libelle,texte) 
VALUES ('SPORT','Pour les amoureux des sensations fortes');
INSERT INTO rubrique(libelle) VALUES ('HUMOUR');
INSERT INTO rubrique(libelle) VALUES ('ART');
INSERT INTO rubrique(libelle) VALUES ('MUSIQUE');
INSERT INTO rubrique(libelle) VALUES ('COULEUR');
INSERT INTO rubrique(libelle) VALUES ('TENNIS');
INSERT INTO rubrique(libelle) VALUES ('AUTOMOBILE');
INSERT INTO rubrique(libelle) VALUES ('HANDBALL');
INSERT INTO rubrique(libelle) VALUES ('GOLF');
INSERT INTO rubrique(libelle) VALUES ('4x500');
INSERT INTO rubrique(libelle) VALUES ('AMOUR');
INSERT INTO rubrique(libelle) VALUES ('INTERNATIONAL');
INSERT INTO rubrique(libelle) VALUES ('MEXIQUE');
INSERT INTO rubrique(libelle) VALUES ('FRANCE');
INSERT INTO rubrique(libelle) VALUES ('TRUMP');
INSERT INTO rubrique(libelle) VALUES ('ROCK');
INSERT INTO rubrique(libelle) VALUES ('RECETTE');
INSERT INTO rubrique(libelle) VALUES ('FAST FOOD');

INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('SPORT','TENNIS');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('SPORT','AUTOMOBILE');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('SPORT','HANDBALL');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('SPORT','GOLF');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('SPORT','4x500');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('INTERNATIONAL','MEXIQUE');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('ART','MUSIQUE');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('ART','COULEUR');
INSERT INTO arbre_rubrique(pere, fils) 
VALUES ('MUSIQUE','ROCK');

INSERT INTO statut_article(libelle) VALUES('en redaction');
INSERT INTO statut_article(libelle) VALUES('soumis');
INSERT INTO statut_article(libelle) VALUES('en relecture');
INSERT INTO statut_article(libelle) VALUES('rejeté');
INSERT INTO statut_article(libelle) VALUES('à réviser');
INSERT INTO statut_article(libelle) VALUES('validé');

INSERT INTO etat_article(libelle) VALUES('actif');
INSERT INTO etat_article(libelle) VALUES('supprimé');

INSERT INTO article(id, auteur, statut, etat, titre, date_creation, honneur)
VALUES(1, 3, 'en redaction','actif','COMMENT LES VENTILLATEUR VENTILLE','2003-01-22', false);
INSERT INTO article(id, auteur, statut, etat, titre, date_creation, date_publication, note_editeur, honneur)
VALUES(666, 3, 'validé','actif','Vous allez être étonné surtout au numero 3','2012-01-22','2014-01-22' , 'imagine comment on va devenir riche et faute a la 3eme ligne', true);
INSERT INTO article(id, auteur, statut, etat, titre, date_creation, honneur)
VALUES(122, 3, 'validé','supprimé','Chanson pour jeune et moins jeune','2003-01-22', false);
INSERT INTO article(id, auteur, statut, etat, titre, date_creation, honneur)
VALUES(2, 333, 'à réviser','actif','Ce que votre marie de vous dit pas','2003-01-22', false);
INSERT INTO article(id, auteur, statut, etat, titre, date_creation, honneur)
VALUES(3, 333, 'à réviser','actif','Les chats sont ils liquident ?','2024-05-22', false);

INSERT INTO rubrique_article(rubrique, article)
VALUES('AMOUR',666);
INSERT INTO rubrique_article(rubrique, article)
VALUES('HUMOUR',666);
INSERT INTO rubrique_article(rubrique, article)
VALUES('SCIENCES',1);
INSERT INTO rubrique_article(rubrique, article)
VALUES('AMOUR',2);
INSERT INTO rubrique_article(rubrique, article)
VALUES('HUMOUR',122);
INSERT INTO rubrique_article(rubrique, article)
VALUES('SCIENCES',3);
INSERT INTO rubrique_article(rubrique, article)
VALUES('POLITIQUE',1);

INSERT INTO lien_article(article1,article2)VALUES (1,3);
INSERT INTO lien_article(article1,article2)VALUES (666,2);

INSERT INTO mot_cle(mot) VALUES('voiture');
INSERT INTO mot_cle(mot) VALUES('velo');
INSERT INTO mot_cle(mot) VALUES('chaussure');
INSERT INTO mot_cle(mot) VALUES('chat');
INSERT INTO mot_cle(mot) VALUES('animal');
INSERT INTO mot_cle(mot) VALUES('legume');
INSERT INTO mot_cle(mot) VALUES('energie');
INSERT INTO mot_cle(mot) VALUES('été');
INSERT INTO mot_cle(mot) VALUES('chaleur');
INSERT INTO mot_cle(mot) VALUES('satisfaction');
INSERT INTO mot_cle(mot) VALUES('chanson');
INSERT INTO mot_cle(mot) VALUES('genre');
INSERT INTO mot_cle(mot) VALUES('cerveau');
INSERT INTO mot_cle(mot) VALUES('relation');

INSERT INTO mot_article(mot,article) VALUES ('chat',3);
INSERT INTO mot_article(mot,article) VALUES ('energie',3);
INSERT INTO mot_article(mot,article) VALUES ('chaleur',3);
INSERT INTO mot_article(mot,article) VALUES ('chaleur',1);
INSERT INTO mot_article(mot,article) VALUES ('été',1);
INSERT INTO mot_article(mot,article) VALUES ('animal',3);
INSERT INTO mot_article(mot,article) VALUES ('satisfaction',666);

INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (1,666,1,'La pate auto former','4 pots de pâte à modeler Play-Doh');
INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (2,666,2,'douceur','important car definition Qualité dun mouvement progressif et aisé, de ce qui fonctionne sans heurt ni bruit.');
INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (3,1,1,'La puissance du moteur','Le moteur du ventilateur est vraiment tres rapide pour assuer une fraicheur inesperer en periode de grande chaleur');
INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (4,1,3,'Une pale optimiser','depuis la creation de lhumanite lhomme cherche a etandre ça domination a travers le monde et le ventilature lui permet de rester sur sa terasse, cela a donc aider a appaiser les desirs de batailles, l ONU a donc travaillé au bon mintient de ce phenomene en optimisant la projection dair pou ogmenter le repos des gens');
INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (5,122,2,'Les chansons des artiste pas populaire','giedré est une grande chanteuse dde musique pour enfant mais plutot pour adulte');
INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (6,2,1,'AMOUR MAIS PAS DE TRAHISON','Votre marie ne vous dit pas qil vous aime car il vous aime mais le repeter tout le temps cest moins marrant...');
INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (7,2,3,'BBQ','Non quand votre homme fait le BBQ ce nest pas pour vous priver de la tache est juste pour vous eviter de sentire la cramer');
INSERT INTO blocs_text(id,article,numero,titre,contenu)
VALUES (8,2,51,'AMOUR MAIS PAS DE TRAHISON','Votre marie ne vous dit pas quil vous aime car il vous aime mais le repeter tout le temps cest moins marrant...');

INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(1,1,2,'Le ventillateur moderne','https://image.image/ventilo');
INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(2,1,4,'ventillateur futuriste','https://image.image/ventilodufutur');
INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(3,666,3,'peinture qui coule','https://image.image/peinture2');
INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(4,666,4,'acoustique eglise','https://image.image/echo');
INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(5,122,1,'Michel en concert','https://image.image/concertmichel');
INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(6,2,2,'lit de mariage','https://image.image/lit');
INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(7,2,4,'contrat','https://image.image/mariage');
INSERT INTO blocs_image(id,article,numero,titre,lien)
VALUES(8,2,6,'2 perssonnes hereuse','https://image.image/deux');

INSERT INTO etat_commentaire (libelle) VALUES('cacher');
INSERT INTO etat_commentaire (libelle) VALUES('supprimé');
INSERT INTO etat_commentaire (libelle) VALUES('normal');
INSERT INTO etat_commentaire (libelle) VALUES('exergue');

INSERT INTO commentaire(id,date_creation,titre,texte,auteur,article,etat)
VALUES(1,'2050-02-05','pas bien','jaime pas ton truc',2,2,'normal');
INSERT INTO commentaire(id,date_creation,titre,texte,auteur,article,etat)
VALUES(2,'2050-02-05','INCROYABLE','jai apris tellement de chose wow',1,2,'exergue');
INSERT INTO commentaire(id,date_creation,titre,texte,auteur,article,etat)
VALUES(3,'2050-02-05','coucou je comprend pas comment le site marche','v',2,1,'cacher');
INSERT INTO commentaire(id,date_creation,titre,texte,auteur,article,etat)
VALUES(4,'2050-02-05','unsulte','encore insulte',1,2,'supprimé');

INSERT INTO action_commentaire(libelle) VALUES ('ecrir');
INSERT INTO action_commentaire(libelle) VALUES ('cacher');
INSERT INTO action_commentaire(libelle) VALUES ('supprimer');
INSERT INTO action_commentaire(libelle) VALUES ('modifier');
INSERT INTO action_commentaire(libelle) VALUES ('decacher');

INSERT INTO log_commentaire(id,temps,objet,users,action)
VALUES (1,'2001-09-29 00:00:00',1,3,'ecrir');
INSERT INTO log_commentaire(id,temps,objet,users,action)
VALUES (2,'2001-09-29 00:00:00',1,3,'modifier');

INSERT INTO action_article(libelle) VALUES ('creer');
INSERT INTO action_article(libelle) VALUES ('supprimer');
INSERT INTO action_article(libelle) VALUES ('valider');
INSERT INTO action_article(libelle) VALUES ('ajouter blocs');
INSERT INTO action_article(libelle) VALUES ('supprimer blocs');
INSERT INTO action_article(libelle) VALUES ('publier');

INSERT INTO log_article(id,temps,objet,users,action)
VALUES (1,'2011-09-20 00:00:00',1,3,'creer');
INSERT INTO log_article(id,temps,objet,users,action)
VALUES (2,'2011-09-29 00:00:00',1,3,'ajouter blocs');


INSERT INTO action_user(libelle) VALUES ('creer compte');
INSERT INTO action_user(libelle) VALUES ('supprimer compte');
INSERT INTO action_user(libelle) VALUES ('ajouter droits');

INSERT INTO log_user(id,temps,objet,users,action)
VALUES (1,'2001-09-29 00:00:00',150,10,'creer compte');
INSERT INTO log_user(id,temps,objet,users,action)
VALUES (2,'2010-09-29 00:00:00',3,10,'ajouter droits');