# Dépendance donctionnelle

## DF

### Users

(pseudo pas unique car dans l'affichage on fera pseudo#id pour différencier)
(aucune autre clé candidate sauf Relation tout clé)
id -> mot_passe
id -> pseudo
id -> nom
id -> prenom
id -> mail
id -> Type => UserType.libelle

### UserType

pas de dépendance

### Droits

pas de dépendance

### DroitsType

pas de dépendance

### DroitUser

pas de dépendance

### Rubrique

libelle -> description

### Arbre rubrique

pas de dépendance

### Statut article

pas de dépendance

### Etat article

pas de dépendance

### Article

(aucune autre clé candidate sauf Relation tout clé)
id -> titre
id -> statut
id -> etat
id -> date de creation
id -> publication
id -> note editeur
id -> honneur

### lien article

pas de dépendance

### mot_clé

pas de dépendance

### motclé_article

pas de dépendance

### blocs texte

id -> titre
id -> numero
id -> contenu

### blocs image

id -> titre
id -> numero
id -> lien

### etatCommentaire

pas de dépendance

### commentaire

(aucune autre clé candidate sauf Relation tout clé)
id -> titre
id -> date creation
id -> titre
id -> texte
id -> etat => EtatCommentaire.libelle
id -> article => article.id

### actionCommentaire

pas de dépendance

### logCommentaire

id -> date
id -> objet => commentaire.id
id -> user => users.id
id -> actionCommentaire.libelle

### Action article

pas de dépendance

### logArticle

id -> date
id -> objet => article.id
id -> user => User.id
id -> actionArticle.libelle

### actionUser

pas de dépendance

### LogUser

id -> date
id -> objet => User.id
id -> user => user.id
id -> action => ActionUser.id

## Validation des NF

### 1NF

Toutes les clés sont atomiques

### 2NF + 3NF

Il n'existe aucune clé (sauf la clé primaire) pouvant déterminer une autre clé d'une meme table.

### BCNF

Les dépendances fonctionnelles sont de la formem C → A avec C étant une clé. Car il n'y aucune redondance d'information dans une table.
