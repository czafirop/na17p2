-- voir tout les articles en redaction requete pour l'editeur --
SELECT *
FROM article
WHERE article.statut = 'en redaction';

-- voir un article avec ses commentaires --

SELECT article.titre, users.pseudo, commentaire.titre, commentaire.auteur
FROM (article INNER JOIN users ON article.auteur=users.id) 
LEFT JOIN commentaire on article.id = commentaire.article
WHERE article.date_publication IS NOT NULL;

-- voir les articles a l'honneur --

SELECT article.id, pseudo, titre, date_publication
FROM article INNER JOIN users ON article.auteur=users.id
WHERE article.honneur=true;

-- voir les articles dans les rubrique --
SELECT rubrique, article.titre, pseudo
FROM (article INNER JOIN users ON article.auteur=users.id)
INNER JOIN rubrique_article ON article.id=rubrique_article.article
ORDER BY rubrique, pseudo;

-- liste des comptes et droit--

SELECT pseudo, nom, prenom, mail , droit_type.droits, droit_user.droits
FROM (users INNER JOIN droit_type ON users.usertype = droit_type.usertype)
LEFT JOIN droit_user ON users.id = droit_user.users
ORDER BY mail;